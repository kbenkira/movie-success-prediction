import os
import re
import json
from collections import defaultdict
from urllib.parse import *
import time
import random
from argparse import ArgumentParser

import requests
import scrapy
from scrapy import Selector
import pandas as pd
import numpy as np

IMDB_BASE_URL = "http://www.imdb.com/title/"
IMDB_GROSS_PAGE = "http://www.imdb.com/title/%s/business?ref_=tt_dt_bus"
REGEX_REFERENCE_CAST = re.compile("nm[0-9]{7}")
REGEX_NB_OSCAR_WINS = re.compile("[0-9]{1,4}")
REGEX_OTHER_WINS = re.compile("([0-9]{1,3}) win")
REGEX_OTHER_NOMINATIONS = re.compile("([0-9]{1,4}) nomination")
REGEX_GROSS = re.compile("[0-9]{0,3}[,\\s]")

def has_child(item):
    return item and item[0]

class Proxy():
    def __init__(self, proxy_conf_path):
        with open(proxy_conf_path, "r") as f:
            proxy_conf = json.load(f)
        
        self.username = proxy_conf["username"]
        self.password = proxy_conf["password"]
        self.hosts = proxy_conf["hosts"]
        self.port = proxy_conf["port"]

    def choose_host(self):
        return random.choice(self.hosts)       

class IMDBScraper():
    
    def __init__(self, base_url, waiting_time, proxy_conf_path):
        self.base_url = base_url
        self.waiting_time = waiting_time
        self.proxy  = Proxy(proxy_conf_path)
     
    def fetch_page(self, to_fetch):
        host = self.proxy.choose_host()
        proxy_config = {'http': 'http://%s:%s@%s:%s' %(self.proxy.username, self.proxy.password, host, self.proxy.port),
                        'https': 'https://%s:%s@%s:%s' %(self.proxy.username, self.proxy.password, host, self.proxy.port)}
        response = requests.get(to_fetch, proxies=proxy_config)
        body = response.content.decode("utf-8")
        return Selector(text=body)

    def get_synopsis(self, response):
        summary_text = response.css("div.summary_text::text").extract()[0].strip()
        return summary_text
    
    def get_principals(self, response):
        principal_cast = response.css("div.credit_summary_item")
        profession_to_ref = defaultdict(set)
        for cast_selector in principal_cast:
            profession = cast_selector.css("h4::text").extract()[0][:-1].lower()
            profession = profession[:-1] if profession[-1] == "s" else profession
            imdb_reference_links = " ".join(cast_selector.css("a::attr(href)").extract())
            for imdb_ref in REGEX_REFERENCE_CAST.finditer(imdb_reference_links):
                profession_to_ref[profession].add(imdb_ref.group())
        return {profession: list(ids) for profession, ids in profession_to_ref.items()}
    
    def get_metascore(self, response):
        metascore_sel = response.css("div.metacriticScore")
        if metascore_sel:
            return float(metascore_sel[0].css("span::text").extract()[0])
        
    def get_critic_reviews(self, response):
        critic_sel = response.css("div.titleReviewBarItem.titleReviewbarItemBorder")
        reviews = {}
        if critic_sel:
            reviews_text = critic_sel.css("span.subText").css("a::text").extract()
            for item in reviews_text:
                reviews[item.split(" ")[1]] = item.split(" ")[0]
        return reviews
        
    def get_awards(self, response):
        result = {}
        awards = response.css('div[id="titleAwardsRanks"]')
        awards = awards.css("span[itemprop='awards']")
        if len(awards):
            for award in awards:
                if award.css("b"):
                    oscar = award.css("b::text").extract()[0]
                    if oscar.strip().lower().startswith("won"):
                        result["nb_oscar_won"] = int(REGEX_NB_OSCAR_WINS.findall(oscar)[0])
                    elif oscar.strip().lower().startswith("nominated"):
                        result["nb_oscar_nominations"] = int(REGEX_NB_OSCAR_WINS.findall(oscar)[0])
                else:
                    wins = award.css("span::text").extract()[0].strip()
                    nb_wins = REGEX_OTHER_WINS.findall(wins)
                    if nb_wins:
                        result["nb_other_wins"] = int(nb_wins[0])
                    nb_nominations = REGEX_OTHER_NOMINATIONS.findall(wins)
                    if nb_nominations:
                        result["nb_other_nominations"] = int(nb_nominations[0])
        return result
    
    def get_cast(self, response):
        # Order matters
        actors = dict()
        cast = response.css("table.cast_list")
        for idx, actor in enumerate(cast.css("td[itemprop='actor']").css("a::attr(href)").extract()):
            ref = REGEX_REFERENCE_CAST.findall(actor)[0]
            actors[ref] = idx+1
        return actors
    
    def get_storyline(self, response):
        storyline = response.css("div[id='titleStoryLine']")
        if storyline:
            plot = storyline.css("div[itemprop='description']")
            if plot:
                return " ".join(plot.css("p::text").extract())

    def get_complete_synopsis(self, ttid):
        url = "http://www.imdb.com/title/%s/plotsummary?ref_=tt_stry_pl#synopsis"
        response = self.fetch_page(url)
        synopsis = response.css("ul[id='plot-synopsis-content']")
        if synopsis:
            plot = synopsis.css("li[class='ipl-zebra-list__item']")
            if plot:
                return " ".join(plot.css("::text").extract())
   
    def get_details(self, response, tconst):
        result = {}
        details = response.css("div[id='titleDetails']")
        for detail_sel in details.css("div[class='txt-block']"):
            detail_sel_title = detail_sel.css("h4::text").extract()
            if not has_child(detail_sel_title):
                continue
            if detail_sel_title[0].lower()[:-1] == "official sites":
                result["official_sites"] = [" ".join(item.split(" ")[1:]) for item in detail_sel.css("a::text").extract() if item.startswith("Official")]
            elif detail_sel_title[0].lower()[:-1] == "country":
                result["countries"] = detail_sel.css("a::text").extract()
            elif detail_sel_title[0].lower()[:-1] == "language":
                result["lang"] = detail_sel.css("a::text").extract()
            elif detail_sel_title[0].lower()[:-1] == "release date":
                release = detail_sel.css("::text").extract()
                if len(release) > 2:
                    result["release_date"] = release[2].strip()
            elif detail_sel_title[0].lower()[:-1] == "budget":
                budget_data = detail_sel.css("::text").extract()
                if len(budget_data) > 2:
                    result["budget"] = budget_data[2].strip()
            elif detail_sel_title[0].lower()[:-1] == "gross":
                gross_data = detail_sel.css("::text").extract()
                if len(gross_data) > 2:
                    result["us_gross"] = gross_data[2].strip()
                result["worldwide_gross"] = self.get_worldwide_gross(tconst)
            elif detail_sel_title[0].lower()[:-1] == "production co":
                result["productors"] = detail_sel.css("span[itemprop='creator']").css("span[itemprop='name']::text").extract()
        return result
   
    def get_worldwide_gross(self, tconst):
        to_fetch = IMDB_GROSS_PAGE % tconst
        selector = self.fetch_page(to_fetch)
        worldwide_gross = self.get_gross(selector)
        return worldwide_gross

    def get_gross(self, selector):
        all_div = selector.css("div[id='tn15content']::text")
        all_gross_revenue = []
        for line in all_div.extract():
            line = line.lower()
            if "worldwide" in line:
                all_gross_revenue.append(line)
        gross = []
        if len(all_gross_revenue):
            for g in all_gross_revenue:
                gross.append(float("".join(REGEX_GROSS.findall(g)).replace(",", "")))
            return max(gross)
        return None

    def extract_movie(self, tconst):
        data = defaultdict(dict)

        # -- Request and Scrapy init
        to_fetch = urljoin(self.base_url, tconst)
        extractor = self.fetch_page(to_fetch)

        # --- synopsis and storyline 
        data["synopsis"] = self.get_synopsis(extractor)
        data["storyline"] = self.get_storyline(extractor)

        # --- principals
        data["principals"] = dict({k: list(v) for k, v in self.get_principals(extractor).items()})

        # --- metascore
        data["metascore"] = self.get_metascore(extractor)

        # --- critics
        data["critics"] = self.get_critic_reviews(extractor)

        # --- awards
        data["awards"] = self.get_awards(extractor)

        # --- cast
        data["casting"] = self.get_cast(extractor)

        # --- details
        data["details"] = self.get_details(extractor, tconst)

        return data

    def process_batch(self, movie_ids, results):
        problems = set()
        count = 0
        movie_ids_queue = set(movie_ids)
        while True:
            try:
                id_ = movie_ids_queue.pop()
                count += 1
                extracted = self.extract_movie(id_)
                synopsis = self.get_complete_synopsis(self, id_)
                extracted["complete_synopsis"] = synopsis
                
                fetched_at = time.time()
                results.append({"movie": id_,
                                "fetched_at": fetched_at,
                                "url": to_fetch, 
                                "data": extracted})
                
                # courtesy waiting time
                time.sleep(random.randint(1, self.waiting_time))
                
                if count % 200 == 0:
                    with open("scraped_data.json", "w") as f:
                        json.dump(results, f)
                    print("Has processed %d items so far, of which %d have been successfully scraped!" % (count, len(results)))
    
            except KeyError:
                print("\n")
                print("Process done with : %d items fetched, %d items successfuly scraped and %d items failed" % (count, len(results), len(problems)))
                with open("scraped_data.json", "w") as f:
                    json.dump(results, f)
                with open("scraped_ids_problem.json", "w") as f:
                    json.dump(list(problems), f)
                return results, problems
                
            except Exception as e:
                print("Raised exception for url %s" % to_fetch)
                print(e)
                problems.add(id_)
                continue

if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument("--config", type=str, help="Path to a scraper config file.", required=True)
    parser.add_argument("--input", type=str, help="Path to csv file with tconst ids.", required=True)
    args = parser.parse_args()

    scraper = IMDBScraper(IMDB_BASE_URL, 2, args.config)
    already_fetched = list()
    if "scraped_data.json" in os.listdir():
        with open("scraped_data.json", "r") as f:
            results = json.load(f)
            already_fetched.extend([r["movie"] for r in results])
    else:
        results = []
    df_movies = pd.read_csv(args.input, sep=";")
    df_movies = df_movies[~df_movies["tconst"].isin(already_fetched)] # remove already fetched movies
    tconsts = np.array(df_movies.sort_values("numVotes", ascending=False)["tconst"]) #sorted list of tconst according to the # of votes.
    print("Start fetching %d movies..." % len(tconsts))
    final_results, problems = scraper.process_batch(tconsts, results)
